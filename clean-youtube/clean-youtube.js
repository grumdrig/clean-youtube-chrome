'use strict';

$(function () { 

    var userInfo = $('#watch-headline-user-info').html(),
        showText = "Show Comments, Favorites, etc.",
        hideText = "Hide Comments, Favorites, etc.";

    $('#watch-headline-user-info').empty().css('height', 0);
    $('#watch-video-container').append('<div id="clean-youtube-userinfo">' + userInfo + 
        '<p id="toggle-comments"><a></a></p>' +
    '</div>');
    $('#watch-main').addClass('hide-comments');
    
    if ($('#watch-main').is(':visible')) {
        $('#toggle-comments a').text(hideText);
        $('#watch-main').removeClass('hide-comments');
    }
    else {
        $('#toggle-comments a').text(showText);
        $('#watch-main').addClass('hide-comments');
    }

    $('#toggle-comments a').live('click', function () {
        if ($('#watch-main').is(':visible')) {
            $('#watch-main').addClass('hide-comments');
            $(this).text(showText);
        }
        else if ($('#watch-main').is(':hidden')) {
            $('#watch-main').removeClass('hide-comments');
            $(this).text(hideText);
        }
    });        
         
    if (window.location.pathname !== '/') {
        $('#logo').attr('src', '').parent().html('<h1 id="clean-youtube-h1">You<span>Tube</span></h1>');
    }
    else {
        $('#logo').attr('src', '').parent().html('<h1 id="clean-youtube-home-h1">You<span>Tube</span></h1>'); 
        $('#masthead-utility').attr('id', 'masthead-utility-clean-youtube-home');
    }
 
}); 